<?php

namespace laylatichy\nano\modules\email\handlers;

use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

interface Handler {
    public function getFrom(): Address;

    public function send(Email $email): void;
}
