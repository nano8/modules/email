---
layout: doc
---

<script setup>
const args = {};
</script>

##### laylatichy\nano\modules\email\events

this event is fired when an email fails to send

to listen for this event

```php
class EmailFailedListener implements NanoEventListener {
    public function handle(EmailFailedEvent $event): void {
        // do something
    }
}

useEvent(EmailFailed::class)->attach(new EmailFailedListener());
```


