<?php

namespace laylatichy\nano\modules\email\engines;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigEngine implements Engine {
    public readonly FilesystemLoader $loader;

    public Environment $twig;

    public string $css;

    public function __construct() {
        $this->loader = new FilesystemLoader(useConfig()->getRoot() . '/email');

        $this->css = file_get_contents(useConfig()->getRoot() . '/assets/css/email.css') ?: '';
    }

    public function setCache(): void {
        $this->twig = new Environment($this->loader, [
            'cache' => useConfig()->getRoot() . '/.cache/email',
        ]);
    }

    public function clearCache(): void {
        // remove all files and directories in cache
        $allFiles = glob(useConfig()->getRoot() . '/.cache/email/*');

        if (is_array($allFiles)) {
            foreach ($allFiles as $file) {
                if (is_file($file)) {
                    unlink($file);
                }

                if (is_dir($file)) {
                    $files = glob($file . '/*');

                    if (is_array($files)) {
                        foreach ($files as $f) {
                            if (is_file($f)) {
                                unlink($f);
                            }
                        }
                    }

                    rmdir($file);
                }
            }
        }
    }

    public function render(string $file, array $data = []): ?string {
        return $this->twig->render("{$file}.twig", $data);
    }

    public function getCss(): string {
        return $this->css;
    }
}
