<?php

use laylatichy\nano\modules\email\EmailModule;
use laylatichy\nano\modules\email\engines\Engine;
use laylatichy\nano\modules\email\handlers\Handler;
use laylatichy\nano\modules\email\messages\EmailMessage;

if (!function_exists('defineEmailEngine')) {
    function defineEmailEngine(Engine $engine): void {
        useNanoModule(EmailModule::class)
            ->email
            ->withEngine($engine);
    }
}

if (!function_exists('defineEmailHandler')) {
    function defineEmailHandler(Handler $handler): void {
        useNanoModule(EmailModule::class)
            ->email
            ->withHandler($handler);
    }
}

if (!function_exists('useEmail')) {
    function useEmail(): EmailMessage {
        return useNanoModule(EmailModule::class)
            ->email
            ->create();
    }
}