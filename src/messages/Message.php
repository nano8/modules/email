<?php

namespace laylatichy\nano\modules\email\messages;

interface Message {
    public function withSubject(string $subject): self;

    public function withHeader(string $header, string $value): self;

    public function from(string $email, string $name): self;

    public function to(string $email, string $name): self;

    public function cc(string $email, string $name): self;

    public function bcc(string $email, string $name): self;

    public function withTemplate(string $file, array $data = []): self;

    public function send(): void;
}
