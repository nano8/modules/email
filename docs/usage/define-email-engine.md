---
layout: doc
---

<script setup>
const args = {
    defineEmailEngine: [
        {
            type: 'Engine',
            name: 'engine',
        },
    ],
};
</script>

##### laylatichy\nano\modules\email\Email

## <Types fn="defineEmailEngine" r="void" :args="args.defineEmailEngine" /> {#defineEmailEngine}

define the email engine that will be used by nano to render email templates

by default smarty is used so if you are fine you can omit this step

to use twig instead

```php
defineEmailEngine(new TwigEngine());
```

engine will look for templates in `{root}/email` directory

smarty file extension is `.tpl` e.g. `email/welcome.tpl`

twig file extension is `.twig` e.g. `email/welcome.twig`

