<?php

namespace laylatichy\nano\modules\email\events;

use laylatichy\nano\events\NanoEvent;

class EmailSent extends NanoEvent {}
