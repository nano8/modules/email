---
layout: doc
---

<script setup>
const args = {
    useEmail: [],
    withSubject: [
        {
            type: 'string',
            name: 'subject',
        },
    ],
    withHeader: [
        {
            type: 'string',
            name: 'header',
        },
        {
            type: 'string',
            name: 'value',
        },
    ],
    from: [
        {
            type: 'string',
            name: 'email',
        },
        {
            type: 'string',
            name: 'name',
        },
    ],
    to: [
        {
            type: 'string',
            name: 'email',
        },
        {
            type: 'string',
            name: 'name',
        },
    ],
    cc: [
        {
            type: 'string',
            name: 'email',
        },
        {
            type: 'string',
            name: 'name',
        },
    ],
    bcc: [
        {
            type: 'string',
            name: 'email',
        },
        {
            type: 'string',
            name: 'name',
        },
    ],
    withTemplate: [
        {
            type: 'string',
            name: 'template',
        },
        {
            type: 'array',
            name: 'data',
        },
    ],
    send: [],
};
</script>

##### laylatichy\nano\modules\email\Email

## usage

`useEmail` is a function that allows you to send emails

## <Types fn="useEmail" r="EmailMessage" :args="args.useEmail" /> {#useEmail}

```php
useEmail()
    ->withSubject('subject')
    ->from('email@example', 'name')
    ->to('email@example', 'name')
    ->withTemplate('account/register', [
        'name' => 'John Doe',
    ])
    ->send();
```

## <Types fn="withSubject" r="EmailMessage" :args="args.withSubject" /> {#withSubject}

```php
useEmail()->withSubject('subject');
```

## <Types fn="withHeader" r="EmailMessage" :args="args.withHeader" /> {#withHeader}

```php
useEmail()->withHeader('header', 'value');
```

## <Types fn="from" r="EmailMessage" :args="args.from" /> {#from}

```php
useEmail()->from('email@example', 'name');
```

## <Types fn="to" r="EmailMessage" :args="args.to" /> {#to}

```php
useEmail()->to('email@example', 'name');
```

## <Types fn="cc" r="EmailMessage" :args="args.cc" /> {#cc}

```php
useEmail()->cc('email@example', 'name');
```

## <Types fn="bcc" r="EmailMessage" :args="args.bcc" /> {#bcc}

```php
useEmail()->bcc('email@example', 'name');
```

## <Types fn="withTemplate" r="EmailMessage" :args="args.withTemplate" /> {#withTemplate}

```php
useEmail()->withTemplate('account/register', [
    'name' => 'John Doe',
]);
```

## <Types fn="send" r="void" :args="args.send" /> {#send}

if no `from` is specified, the default address from handler will be used

```php
useEmail()->send();
```

