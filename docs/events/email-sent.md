---
layout: doc
---

<script setup>
const args = {};
</script>

##### laylatichy\nano\modules\email\events

this event is fired when an email is sent

to listen for this event

```php
class EmailSentListener implements NanoEventListener {
    public function handle(EmailSentEvent $event): void {
        // do something
    }
}

useEvent(EmailSent::class)->attach(new EmailSentListener());
```


