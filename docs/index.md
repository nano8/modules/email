---
layout: home

hero:
    name:    nano/modules/email
    tagline: email module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/email is a simple and minimal email module for nano
    -   title:   easy to use
        details: |
                 nano/modules/email is easy to use, and has a simple and minimal api
---
