<?php

namespace laylatichy\nano\modules\email;

use laylatichy\nano\modules\email\events\EmailFailed;
use laylatichy\nano\modules\email\events\EmailSent;
use laylatichy\nano\modules\NanoModule;

use laylatichy\nano\Nano;

class EmailModule implements NanoModule {
    public Email $email;

    public function __construct() {}

    public function register(Nano $nano): void {
        if (isset($this->email)) {
            useNanoException('email module already registered');
        }

        $this->email = new Email();

        $this->load();

        $this->events($nano);
    }

    private function load(): void {
        $file = useConfig()->getRoot() . '/../.config/email.php';

        if (file_exists($file)) {
            require $file;
        }
    }

    private function events(Nano $nano): void {
        $nano
            ->withEvent(new EmailSent())
            ->withEvent(new EmailFailed());
    }
}
