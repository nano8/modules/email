<?php

namespace laylatichy\nano\modules\email;

use laylatichy\nano\modules\email\engines\Engine;
use laylatichy\nano\modules\email\engines\SmartyEngine;
use laylatichy\nano\modules\email\handlers\Handler;
use laylatichy\nano\modules\email\messages\EmailMessage;

class Email {
    public Handler $handler;

    public function __construct(
        public Engine $engine = new SmartyEngine(),
    ) {
        $this->engine->setCache();
    }

    public function withEngine(Engine $engine): void {
        $this->engine = $engine;

        $this->engine->setCache();
    }

    public function withHandler(Handler $handler): void {
        $this->handler = $handler;
    }

    public function create(): EmailMessage {
        return new EmailMessage($this->engine, $this->handler);
    }
}
