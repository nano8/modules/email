<?php

namespace laylatichy\nano\modules\email\events;

use laylatichy\nano\events\NanoEventMessage;
use laylatichy\nano\modules\email\messages\EmailMessage;

class EmailSentEvent implements NanoEventMessage {
    public function __construct(
        public EmailMessage $email,
    ) {}
}
