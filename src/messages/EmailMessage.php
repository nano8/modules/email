<?php

namespace laylatichy\nano\modules\email\messages;

use Exception;
use Html2Text\Html2Text;
use laylatichy\nano\modules\email\engines\Engine;
use laylatichy\nano\modules\email\events\EmailFailed;
use laylatichy\nano\modules\email\events\EmailFailedEvent;
use laylatichy\nano\modules\email\events\EmailSent;
use laylatichy\nano\modules\email\events\EmailSentEvent;
use laylatichy\nano\modules\email\handlers\Handler;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

use function Pinky\transformString;

class EmailMessage implements Message {
    public string $tpl;

    public string $body;

    public function __construct(
        private readonly Engine $engine,
        private readonly Handler $handler,
        public readonly Email $message = new Email()
    ) {}

    public function withSubject(string $subject): self {
        $this->message->subject($subject);

        return $this;
    }

    public function withHeader(string $header, string $value): self {
        $headers = $this->message->getHeaders();

        $headers->remove('X-SES-CONFIGURATION-SET');
        $headers->remove('X-SES-MESSAGE-TAGS');

        $headers->addTextHeader('X-SES-CONFIGURATION-SET', 'default');
        $headers->addTextHeader('X-SES-MESSAGE-TAGS', $header . '=' . $value);

        return $this;
    }

    public function from(string $email, string $name): self {
        $this->message->from(new Address($email, $name));

        return $this;
    }

    public function to(string $email, string $name): self {
        $this->message->to(new Address($email, $name));

        return $this;
    }

    public function cc(string $email, string $name): self {
        $this->message->cc(new Address($email, $name));

        return $this;
    }

    public function bcc(string $email, string $name): self {
        $this->message->bcc(new Address($email, $name));

        return $this;
    }

    public function withTemplate(string $file, array $data = []): self {
        $this->tpl = $file;

        $html = $this->engine->render($this->tpl, $data);

        if (is_string($html)) {
            $transformed = transformString($html)->saveHTML();
            $html        = is_string($transformed) ? $transformed : '';
            $this->body  = (new CssToInlineStyles())->convert($html, $this->engine->getCss());

            $this->message->html($this->body)
                ->text((new Html2Text($this->body))->getText());
        }

        return $this;
    }

    public function send(): void {
        if (count($this->message->getFrom()) === 0) {
            $this->message->from($this->handler->getFrom());
        }

        try {
            $this->handler->send($this->message);

            useEvent(EmailSent::class)
                ->dispatch(new EmailSentEvent($this));
        } catch (Exception $e) {
            useEvent(EmailFailed::class)
                ->dispatch(new EmailFailedEvent($this, $e));
        }
    }
}
