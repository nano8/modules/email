<?php

namespace laylatichy\nano\modules\email\handlers;

use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class SymfonyMailerHandler implements Handler {
    private readonly Mailer $mailer;

    private readonly EsmtpTransport $transport;

    private readonly Address $from;

    public function __construct(
        string $host,
        int $port,
        string $user,
        string $password,
        bool $tls,
        string $email,
        string $name,
    ) {
        $this->transport = (new EsmtpTransport($host, $port, $tls))
            ->setUsername($user)
            ->setPassword($password);

        $this->mailer = new Mailer($this->transport);

        $this->from = new Address($email, $name);
    }

    public function getFrom(): Address {
        return $this->from;
    }

    public function send(Email $email): void {
        $this->mailer->send($email);
    }
}
