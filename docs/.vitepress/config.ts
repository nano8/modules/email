import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:   true,
    sitemap:       {
        hostname: 'https://nano8.gitlab.io/modules/email/',
    },
    title:         'nano/modules/email',
    titleTemplate: 'nano/modules/email - documentation',
    description:   'email module for nano framework',
    base:          '/modules/email/',
    themeConfig:   {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'defineEmailEngine',
                        link: '/usage/define-email-engine',
                    },
                    {
                        text: 'defineEmailHandler',
                        link: '/usage/define-email-handler',
                    },
                    {
                        text: 'useEmail',
                        link: '/usage/use-email',
                    },
                ],
            },
            {
                text:  'events',
                items: [
                    {
                        text: 'EmailSent',
                        link: '/events/email-sent',
                    },
                    {
                        text: 'EmailFailed',
                        link: '/events/email-failed',
                    },
                ],
            },
            {
                text: 'nano documentation',
                link: 'https://nano.laylatichy.com',
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/modules/email',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/modules/email/-/edit/dev/docs/:path',
        },
    },
});
