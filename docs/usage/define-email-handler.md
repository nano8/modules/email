---
layout: doc
---

<script setup>
const args = {
    defineEmailHandler: [
        {
            type: 'Handler',
            name: 'handler',
        },
    ],
    SymfonyMailerHandler: [
        {
            type: 'string',
            name: 'host',
        },
        {
            type: 'int',
            name: 'port',
        },
        {
            type: 'string',
            name: 'user',
        },
        {
            type: 'string',
            name: 'password',
        },
        {
            type: 'bool',
            name: 'tls',
        },
        {
            type: 'string',
            name: 'email',
        },
        {
            type: 'string',
            name: 'name',
        },
    ],
};
</script>

##### laylatichy\nano\modules\email\Email

## <Types fn="defineEmailHandler" r="void" :args="args.defineEmailHandler" /> {#defineEmailHandler}

define the email handler that will be used to send emails

```php
defineEmailHandler(new EmailHandler());
```

## <Types fn="SymfonyMailerHandler" r="SymfonyMailerHandler" :args="args.SymfonyMailerHandler" /> {#SymfonyMailerHandler}

```php
use laylatichy\nano\modules\email\Email;

defineEmailHandler(new SymfonyMailerHandler(
    'smtp.gmail.com',
    465,
    'user',
    'password',
    true,
    'from@example.com',
    'name',
));
```

