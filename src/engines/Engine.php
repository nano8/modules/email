<?php

namespace laylatichy\nano\modules\email\engines;

interface Engine {
    public function setCache(): void;

    public function clearCache(): void;

    public function render(string $file, array $data = []): ?string;

    public function getCss(): string;
}
