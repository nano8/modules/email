<?php

namespace laylatichy\nano\modules\email\events;

use Exception;
use laylatichy\nano\events\NanoEventMessage;
use laylatichy\nano\modules\email\messages\EmailMessage;

class EmailFailedEvent implements NanoEventMessage {
    public function __construct(
        public EmailMessage $email,
        public Exception $exception,
    ) {}
}
