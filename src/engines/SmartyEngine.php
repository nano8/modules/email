<?php

namespace laylatichy\nano\modules\email\engines;

use Smarty\Smarty;

class SmartyEngine implements Engine {
    private string $css;

    public function __construct(
        private readonly Smarty $smarty = new Smarty(),
    ) {
        $this->css = file_get_contents(useConfig()->getRoot() . '/assets/css/email.css') ?: '';
    }

    public function setCache(): void {
        $this->smarty->caching = Smarty::CACHING_OFF;
        $this->smarty->setCompileDir(useConfig()->getRoot() . '/.cache/email/compile');
        $this->smarty->setCacheDir(useConfig()->getRoot() . '/.cache/email/cache');
        $this->smarty->setTemplateDir(useConfig()->getRoot() . '/email');
    }

    public function clearCache(): void {
        $this->smarty->clearAllCache();
        $this->smarty->clearCompiledTemplate();
    }

    public function render(string $file, array $data = []): ?string {
        $this->smarty->assign($data);

        $tpl = $this->smarty->fetch("{$file}.tpl");

        $this->smarty->clearAllAssign();

        return $tpl;
    }

    public function getCss(): string {
        return $this->css;
    }
}
