---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-email
```

## registering module

```php
use laylatichy\nano\modules\email\EmailModule;

useNano()->withModule(new EmailModule());

// define your handlers here or in a .config/email.php file
```

## adding css

by default engines will look for a css file in 

`{root}/assets/css/email.css` 

to inline it in the email




